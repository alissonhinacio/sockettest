#include <opencv2/opencv.hpp>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

int main() {
    // Create socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
    {
        std::cerr << "Error: Socket creation failed" << std::endl;
        return -1;
    }

    // Setup address for the middleman
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(12345); // Middleman's port number
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // Middleman's address

    // Connect to the middleman
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    {
        std::cerr << "Error: Connection to middleman failed" << std::endl;
        return -1;
    }

    cv::Mat frame = cv::Mat::zeros(480, 640, CV_8UC3); // Assuming 640x480 resolution

    while (true) 
    {
        // Receive frame from the middleman
        recv(sockfd, frame.data, frame.total() * frame.elemSize(), MSG_WAITALL);

        // Draw a line in the middle of the frame
        cv::line(frame, cv::Point(frame.cols / 2, 0), cv::Point(frame.cols / 2, frame.rows), cv::Scalar(0, 255, 0), 2);

        // Display the frame
        cv::imshow("Client - Received Frame", frame);

        // Break on ESC key
        if (cv::waitKey(30) == 27) break;
    }

    // Cleanup
    close(sockfd);

    return 0;
}
