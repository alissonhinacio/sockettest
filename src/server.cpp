#include <opencv2/opencv.hpp>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <MessageBroker/MessageBroker.hpp>

using namespace cv;
using namespace std;

void startCamera(VideoCapture &cap)
{
    if (!cap.isOpened())
    {
        cap.open(0); // Open default camera
        if (!cap.isOpened())
        {
            cerr << "Error: Webcam cannot be accessed" << endl;
        }
    }
}

void stopCamera(VideoCapture &cap)
{
    if (cap.isOpened())
    {
        cap.release(); // Release the camera
    }
}

int main()
{
    // Initialize webcam
    VideoCapture cap(0);
    MessageBroker mbroker;
    bool run_ = false;

    messageStructSender message;

    std::cout << "entrou aqui??\n";

    mbroker.setEventToRead(mbsys::SYS_CAMERA_SERVER);

    if (!mbroker.connectToServer())
    {
        return 0;
    }

    if (!cap.isOpened())
    {
        cerr << "Error: Webcam cannot be accessed" << endl;
        return -1;
    }

    // Create socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        cerr << "Error: Socket creation failed" << endl;
        return -1;
    }

    // Setup address for the middleman
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(12345);                  // Port number for the middleman
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // Middleman's address

    // Connect to the middleman
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        cerr << "Error: Connection to middleman failed" << endl;
        return -1;
    }

    Mat frame;
    while (true)
    {

        MessageBroker::MessageValue ret = mbroker.runClient(message);

        switch (ret)
        {
        case MessageBroker::ERROR:
        {
            std::cout << "server goes down kk\n";
            mbroker.tryReconnectToServer(); // Handle error
            break;
        }
        case MessageBroker::VALID:
        {
            if (message.mbctrl_ == mbctrl::SYS_STOP_CAMERA)
            {
                std::cout << "stop cammera\n";
                run_ = false;
            }
            else if (message.mbctrl_ == mbctrl::SYS_START_CAMERA)
            {
                std::cout << "start cammera\n";
                run_ = true;
            }
            else
            {
                // do nothing
            }
            break;
        }
        case MessageBroker::UNVALID:
        case MessageBroker::NO_MESSAGE_RECEIVED:
        default:
            break;
        }

        if (run_)
        {
            if (!cap.isOpened())
            {
                startCamera(cap); // Start the camera if not already started
            }
            cap >> frame; // Capture frame-by-frame
            // ... send frame ...
        }
        else
        {
            stopCamera(cap); // Stop the camera
        }
        // Delay for demonstration purposes
        waitKey(30);
    }

    // Cleanup
    stopCamera(cap); // Ensure the camera is stopped on exit
    close(sockfd);

    return 0;
}
