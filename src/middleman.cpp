#include <opencv2/opencv.hpp>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int listener, client_socket, max_sd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    fd_set readfds;
    vector<int> client_sockets;

    // Creating socket file descriptor
    if ((listener = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attaching socket to the port 12345
    if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(12345);

    // Bind the socket to the address
    if (bind(listener, (struct sockaddr *)&address, sizeof(address)) < 0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(listener, 3) < 0) 
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    char buffer[921600]; // Buffer to store frame data (adjust size as needed)

    while (true) 
    {
        FD_ZERO(&readfds);

        // Add listener to set
        FD_SET(listener, &readfds);
        max_sd = listener;

        // Add child sockets to set
        for (int sd : client_sockets) 
        {
            FD_SET(sd, &readfds);
            if (sd > max_sd) max_sd = sd;
        }

        // Wait for an activity on one of the sockets
        int activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if (activity < 0) 
        {
            perror("select error");
            continue;
        }

        // If something happened on the listener socket, then its an incoming connection
        if (FD_ISSET(listener, &readfds)) 
        {
            if ((new_socket = accept(listener, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) 
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }

            // Add new socket to array of sockets
            client_sockets.push_back(new_socket);
        }

        // Else it's some IO operation on some other socket
        for (int sd : client_sockets) 
        {
            if (FD_ISSET(sd, &readfds)) 
            {
                // Check if it was for closing, and also read the incoming message
                int valread = read(sd, buffer, sizeof(buffer));
                if (valread == 0) 
                {
                    // Some client disconnected, close the socket and mark as 0 in list for reuse
                    close(sd);
                    client_sockets.erase(remove(client_sockets.begin(), client_sockets.end(), sd), client_sockets.end());
                } else 
                {
                    // Echo back the message that came in to all clients
                    for (int out_sd : client_sockets) 
                    {
                        if (out_sd != sd) 
                        { 
                            // Do not send back to the sender
                            send(out_sd, buffer, valread, 0);
                        }
                    }
                }
            }
        }
    }

    return 0;
}
